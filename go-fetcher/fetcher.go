package fetcher

import (
    "log"
    "github.com/golang/protobuf/proto"
    protoFetcher "./fetcher.proto"
    "net/http"
)

func (f *FetcherClient)  SendRequest(stream protoFetcher.Request) error {
    request := http.Request{
        Method: stream.method,
        URL: stream.url,
        Header: stream.header,
    }

    ch <- stream http.Request
    

